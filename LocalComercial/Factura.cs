﻿using System;
using System.Collections.Generic;

namespace CarritoDeCompras
{
    public class Factura
    {

        public decimal SubTotal { get; set; }
        public decimal Descuento { get; set; }
        public decimal Total { get; set; }

        public Factura()
        {
            this.Detalle = new List<DetalleFactura>();
        }

        //Cabecera de Factura
        public CabeceraFactura Cabecera { get; set; }

        //Detalle de carrito
        //Lista de detalle de carrito (Producto y la cantidad)

        public List<DetalleFactura> Detalle { get; set; }


        public void CalcularSubtotal()
        {
            //Acumulador o sumador
            decimal subtotal = 0;
            foreach (DetalleFactura item in this.Detalle)
            {
                subtotal = subtotal + (item.Cantidad * item.ProductoCarrito.Precio);
                if (item.Cantidad>10)
                {
                    this.SubTotal= subtotal*0.1;
                }
            }

            this.SubTotal = subtotal;
        }
        //Parte del codigo Si la cantidad de productos es mayor a 10 entonces se hace un 10% de descuento al valor de la factura

        public void CalcularDescuentoProducto()

        {
            DetalleFactura;
            foreach (DetalleFactura item in this.Detalle)
            {
                if (item.Cantidad>10)
            {
                this.Descuento = this.SubTotal * 0.1;
            }
            else
            {
                this.Descuento = 0;
            }
           
        }

        public void CalcularTotal2()
        {
            this.Total = this.SubTotal - this.Descuento;
        }

             public void CalcularDescuentoProducto()

        {
            DetalleFactura;
            foreach (DetalleFactura item in this.Detalle)
            {
                if (item.Cantidad>10)
            {
                this.Descuento = this.SubTotal * 0.1;
            }
            else
            {
                this.Descuento = 0;
            }
           
        }

    }
}
